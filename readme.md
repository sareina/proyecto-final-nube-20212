# PROYECTO LEY ANTI CORRUPCIÓN 

Esta proyecto permite tener una plataforma en línea 
para que los ciudadanos puedan hacer verificación de 
los casos de anticorrupción en Colombia.

## CURSO

Arquitectura De Soluciones En La Nube - 2021-2

## INTEGRANTES

- Valentina Aldana Tabares
- Santiago Ávila Reina
- Mariapaula Rodriguez Castañeda

## HERRAMIENTAS

- Python
- Django
- Visual Studio Code
- Html
- CSS
- Markdown
- AWS